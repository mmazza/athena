#
# File specifying the location of MadGraph to use.
#

set( MADGRAPH5AMC_LCGVERSION 2.6.7 )
set( MADGRAPH5AMC_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/madgraph5amc/${MADGRAPH5AMC_LCGVERSION}/${LCG_PLATFORM} )
